package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.IAsignadoDAO;
import com.example.demo.dto.Asignado;


@Service
public class AsignadoServiceImpl implements IAsignadoService {

	@Autowired
	IAsignadoDAO asignadoDAO;
	
	@Override
	public List<Asignado> listarAsignado() {
		return asignadoDAO.findAll();
	}

	@Override
	public Asignado guardarAsignado(Asignado asignado) {
		return asignadoDAO.save(asignado);
	}

	@Override
	public Asignado AsignadoXID(int id) {
		return asignadoDAO.findById(id).get();
	}

	@Override
	public Asignado actualizarAsignado(Asignado asignado) {
		return asignadoDAO.save(asignado);
	}

	@Override
	public void eliminarAsignado(int id) {
		asignadoDAO.deleteById(id);
		
	}

}
