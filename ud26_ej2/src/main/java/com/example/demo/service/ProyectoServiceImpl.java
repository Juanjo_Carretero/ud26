package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.IProyectoDAO;
import com.example.demo.dto.Proyecto;

@Service
public class ProyectoServiceImpl implements IProyectoService{

	@Autowired
	IProyectoDAO proyectoDAO;
	
	@Override
	public List<Proyecto> listarProyecto() {
		return proyectoDAO.findAll();
	}

	@Override
	public Proyecto guardarProyecto(Proyecto proyecto) {
		return proyectoDAO.save(proyecto);
	}

	@Override
	public Proyecto proyectoXID(char id) {
		return proyectoDAO.findById(id).get();
	}

	@Override
	public Proyecto actualizarProyecto(Proyecto proyecto) {
		return proyectoDAO.save(proyecto);
	}

	@Override
	public void eliminarProyecto(char id) {
		proyectoDAO.deleteById(id);
		
	}

}
