package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.Proyecto;
import com.example.demo.service.CientificoServiceImpl;
import com.example.demo.service.ProyectoServiceImpl;


@RestController
@RequestMapping("/api")
public class ProyectoController {

	@Autowired
	ProyectoServiceImpl proyectoService;
	
	@GetMapping("/proyecto")
	public List<Proyecto> listarProyecto(){
		return proyectoService.listarProyecto();
	}
	
	
	@PostMapping("/proyecto")
	public Proyecto salvarProyecto(@RequestBody Proyecto proyecto) {
		
		return proyectoService.guardarProyecto(proyecto);
	}
	
	
	@GetMapping("/proyecto/{id}")
	public Proyecto cursoXID(@PathVariable(name="id") char id) {
		
		Proyecto proyecto_xid= new Proyecto();
		
		proyecto_xid=proyectoService.proyectoXID(id);
		
		System.out.println("Proveedor XID: "+proyecto_xid);
		
		return proyecto_xid;
	}
	
	@PutMapping("/proyecto/{id}")
	public Proyecto actualizarCurso(@PathVariable(name="id")char id,@RequestBody Proyecto proyecto) {
		
		Proyecto proyecto_seleccionado= new Proyecto();
		Proyecto proyecto_actualizado= new Proyecto();
		
		proyecto_seleccionado= proyectoService.proyectoXID(id);
		
		proyecto_seleccionado.setNombre(proyecto.getNombre());
		proyecto_seleccionado.setHoras(proyecto.getHoras());
		
		proyecto_actualizado = proyectoService.actualizarProyecto(proyecto);
		
		System.out.println("El proyecto actualizado es: "+ proyecto_actualizado);
		
		return proyecto_actualizado;
	}
	
	@DeleteMapping("/proyecto/{id}")
	public void eleiminarCurso(@PathVariable(name="id")char id) {
		proyectoService.eliminarProyecto(id);
	}
	
}
