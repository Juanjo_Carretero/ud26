package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.Asignado;
import com.example.demo.service.AsignadoServiceImpl;


@RestController
@RequestMapping("/api")
public class AsignadoController {

	@Autowired
	AsignadoServiceImpl asignadoService;
	
	
	@GetMapping("/asignado")
	public List<Asignado> listarAsignado(){
		return asignadoService.listarAsignado();
	}
	
	
	@PostMapping("/asignado")
	public Asignado salvaAsignado(@RequestBody Asignado piezaProveedor) {
		
		return asignadoService.guardarAsignado(piezaProveedor);
	}
	
	
	@GetMapping("/asignado/{id}")
	public Asignado asignadoXID(@PathVariable(name="id") int id) {
		
		Asignado asignado_xid= new Asignado();
		
		asignado_xid=asignadoService.AsignadoXID(id);
		
		System.out.println("Asignado XID: "+asignado_xid);
		
		return asignado_xid;
	}
	
	@PutMapping("/asignado/{id}")
	public Asignado actualizarAsignado(@PathVariable(name="id")int id,@RequestBody Asignado asignado) {
		
		Asignado asignado_seleccionado= new Asignado();
		Asignado asignado_actualizado= new Asignado();
		
		asignado_seleccionado= asignadoService.AsignadoXID(id);
		
		
		asignado_seleccionado.setCientifico(asignado.getCientifico());
		asignado_seleccionado.setProyecto(asignado.getProyecto());
		
		asignado_actualizado = asignadoService.actualizarAsignado(asignado);
		
		System.out.println("El RegistroCurso actualizado es: "+ asignado_actualizado);
		
		return asignado_actualizado;
	}
	
	@DeleteMapping("/asignado/{id}")
	public void eleiminarAsignado(@PathVariable(name="id")int id) {
		asignadoService.eliminarAsignado(id);;
	}
}
