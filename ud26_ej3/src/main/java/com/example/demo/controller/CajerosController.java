package com.example.demo.controller;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.Cajero;
import com.example.demo.service.CajeroServiceImpl;
import com.example.demo.service.ProductoServiceImpl;


@RestController
@RequestMapping("/api")
public class CajerosController {

	@Autowired
	CajeroServiceImpl cajeroService;
	
	@GetMapping("/cajero")
	public List<Cajero> listarCajero(){
		return cajeroService.listarCajero();
	}
	
	
	@PostMapping("/cajero")
	public Cajero salvarCajero(@RequestBody Cajero cajero) {
		
		return cajeroService.guardarCajero(cajero);
	}
	
	
	@GetMapping("/cajero/{codigo}")
	public Cajero CajeroXID(@PathVariable(name="codigo") int codigo) {
		
		Cajero cajero_xid= new Cajero();
		
		cajero_xid=cajeroService.CajeroXID(codigo);
		
		System.out.println("Cajero XID: " + cajero_xid);
		
		return cajero_xid;
	}
	
	@PutMapping("/cajero/{codigo}")
	public Cajero actualizarCajero(@PathVariable(name="codigo")int codigo,@RequestBody Cajero cajero) {
		
		Cajero cajero_seleccionado= new Cajero();
		Cajero cajero_actualizado= new Cajero();
		
		cajero_seleccionado= cajeroService.CajeroXID(codigo);
		
		cajero_seleccionado.setNomApels(cajero.getNomApels());
		
		cajero_actualizado = cajeroService.actualizarCajero(cajero_actualizado);
		
		System.out.println("La pieza actualizada es: "+ cajero_actualizado);
		
		return cajero_actualizado;
	}
	
	@DeleteMapping("/cajero/{codigo}")
	public void eliminarCajero(@PathVariable(name="codigo")int codigo) {
		cajeroService.eliminarCajero(codigo);
	}
}
