package com.example.demo.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;

@Entity
@Table(name="maquinasregistradoras")
public class MaquinaRegistradora {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="codigo")
	private int codigo;
	@Column(name="piso")
	private String piso;

	@OneToMany
	@Column(name="maquina")
	private List<Venta> maquina;


	public MaquinaRegistradora() {
	}
	


	public MaquinaRegistradora(int codigo, String piso, List<Venta> maquina) {
		super();
		this.codigo = codigo;
		this.piso = piso;
		this.maquina = maquina;
	}



	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getPiso() {
		return piso;
	}

	public void setPiso(String piso) {
		this.piso = piso;
	}

	public List<Venta> getMaquina() {
		return maquina;
	}
	
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "Venta")
	public void setMaquina(List<Venta> maquina) {
		this.maquina = maquina;
	}



	@Override
	public String toString() {
		return "MaquinaRegistradora [codigo=" + codigo + ", piso=" + piso + "]";
	}



	

	
	
		
}
