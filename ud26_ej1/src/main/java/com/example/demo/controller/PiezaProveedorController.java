package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.PiezaProveedor;
import com.example.demo.service.PiezaProveedorServiceImpl;


@RestController
@RequestMapping("/api")
public class PiezaProveedorController {

	@Autowired
	PiezaProveedorServiceImpl piezaProveedorService;
	
	
	@GetMapping("/piezaProveedores")
	public List<PiezaProveedor> listarPiezaProveedor(){
		return piezaProveedorService.listarPiezaProveedor();
	}
	
	
	@PostMapping("/piezaProveedores")
	public PiezaProveedor salvaPiezaProveedor(@RequestBody PiezaProveedor piezaProveedor) {
		
		return piezaProveedorService.guardarPiezaProveedor(piezaProveedor);
	}
	
	
	@GetMapping("/piezaProveedores/{id}")
	public PiezaProveedor piezaProveedorXID(@PathVariable(name="id") int id) {
		
		PiezaProveedor RegistroCurso_xid= new PiezaProveedor();
		
		RegistroCurso_xid=piezaProveedorService.piezaProveedorXID(id);
		
		System.out.println("RegistroCurso XID: "+RegistroCurso_xid);
		
		return RegistroCurso_xid;
	}
	
	@PutMapping("/piezaProveedores/{id}")
	public PiezaProveedor actualizarPiezaProveedor(@PathVariable(name="id")int id,@RequestBody PiezaProveedor piezaProveedor) {
		
		PiezaProveedor piezaProveedor_seleccionado= new PiezaProveedor();
		PiezaProveedor piezaProveedor_actualizado= new PiezaProveedor();
		
		piezaProveedor_seleccionado= piezaProveedorService.piezaProveedorXID(id);
		
		
		piezaProveedor_seleccionado.setPieza(piezaProveedor.getPieza());
		piezaProveedor_seleccionado.setProveedor(piezaProveedor.getProveedor());
		piezaProveedor_seleccionado.setPrecio(piezaProveedor.getPrecio());
		
		piezaProveedor_actualizado = piezaProveedorService.actualizarPiezaProveedor(piezaProveedor);
		
		System.out.println("El RegistroCurso actualizado es: "+ piezaProveedor_actualizado);
		
		return piezaProveedor_actualizado;
	}
	
	@DeleteMapping("/piezaProveedores/{id}")
	public void eleiminarPiezaProveedor(@PathVariable(name="id")int id) {
		piezaProveedorService.eliminarPiezaProveedor(id);;
	}
}
