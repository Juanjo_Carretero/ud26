package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.PiezaProveedor;

public interface IPiezaProveedorService {

	//Metodos del CRUD
	public List<PiezaProveedor> listarPiezaProveedor(); //Listar All 
	
	public PiezaProveedor guardarPiezaProveedor(PiezaProveedor piezaProveedor);	//Guarda un RegistroCurso CREATE
	
	public PiezaProveedor piezaProveedorXID(int id); //Leer datos de un RegistroCurso READ
	
	public PiezaProveedor actualizarPiezaProveedor(PiezaProveedor piezaProveedor); //Actualiza datos del RegistroCurso UPDATE
	
	public void eliminarPiezaProveedor(int id);// Elimina el RegistroCurso DELETE
}
