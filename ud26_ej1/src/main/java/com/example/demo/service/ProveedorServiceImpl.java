package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.IProveedorDAO;
import com.example.demo.dto.Proveedor;

@Service
public class ProveedorServiceImpl implements IProveedorService{

	@Autowired
	IProveedorDAO proveedorDao;
	
	
	@Override
	public List<Proveedor> listarProveedores() {
		return proveedorDao.findAll();
	}

	@Override
	public Proveedor guardarProveedor(Proveedor proveedor) {
		return proveedorDao.save(proveedor);
	}

	@Override
	public Proveedor proveedorXID(String id) {
		return proveedorDao.findById(id).get();
	}

	@Override
	public Proveedor actualizarProveedor(Proveedor proveedor) {
		return proveedorDao.save(proveedor);
	}

	@Override
	public void eliminarProveedor(String id) {
		proveedorDao.deleteById(id);
		
	}

}
