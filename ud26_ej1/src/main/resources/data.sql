DROP TABLE IF EXISTS piezas;
DROP TABLE IF EXISTS proveedores;
DROP TABLE IF EXISTS piezas_proveedores;

CREATE TABLE `ud26-1`.`piezas` (
  `codigo` INT NOT NULL,
  `nombre` NVARCHAR(100) NULL,
  PRIMARY KEY (`codigo`));
  
  CREATE TABLE `ud26-1`.`proveedores` (
  `id` CHAR(4) NOT NULL,
  `nombre` NVARCHAR(100) NULL,
  PRIMARY KEY (`id`));
  
  CREATE TABLE `ud26-1`.`piezas_proveedores` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `codigo_pieza` INT NOT NULL,
  `id_proveedor` CHAR(4) NOT NULL,
  `precio` INT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `codigo_pieza`
    FOREIGN KEY (`codigo_pieza`)
    REFERENCES `ud26-1`.`piezas` (`codigo`),
  CONSTRAINT `id_proveedores`
    FOREIGN KEY (`id_proveedor`)
    REFERENCES `ud26-1`.`proveedores` (`id`));
    
INSERT INTO `ud26-1`.`piezas` (`codigo`, `nombre`) VALUES ('1', 'Tornillo');
INSERT INTO `ud26-1`.`piezas` (`codigo`, `nombre`) VALUES ('2', 'Tuerca');
INSERT INTO `ud26-1`.`piezas` (`codigo`, `nombre`) VALUES ('3', 'Destornillador');

INSERT INTO `ud26-1`.`proveedores` (`id`, `nombre`) VALUES ('1', 'JuanJo');
INSERT INTO `ud26-1`.`proveedores` (`id`, `nombre`) VALUES ('2', 'Marc');
INSERT INTO `ud26-1`.`proveedores` (`id`, `nombre`) VALUES ('3', 'Pepito');

INSERT INTO `ud26-1`.`piezas_proveedores` (`codigo_pieza`, `id_proveedor`, `precio`) VALUES ('1', '1', '50');
INSERT INTO `ud26-1`.`piezas_proveedores` (`codigo_pieza`, `id_proveedor`, `precio`) VALUES ('2', '2', '20');
INSERT INTO `ud26-1`.`piezas_proveedores` (`codigo_pieza`, `id_proveedor`, `precio`) VALUES ('3', '3', '15');