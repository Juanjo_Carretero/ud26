package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.Facultad;
import com.example.demo.service.FacultadServiceImpl;

@RestController
@RequestMapping("/api")
public class FacultadController {

		@Autowired
		FacultadServiceImpl facultadService;
		
		@GetMapping("/facultad")
		public List<Facultad> listarFacultades(){
			return facultadService.listarFacultad();
		}
		
		
		@PostMapping("/facultad")
		public Facultad salvarFacultad(@RequestBody Facultad facultad) {
			
			return facultadService.guardarFacultad(facultad);
		}
		
		
		@GetMapping("/facultad/{codigo}")
		public Facultad facultadXID(@PathVariable(name="codigo") int id) {
			
			Facultad pieza_xid= new Facultad();
			
			pieza_xid=facultadService.facultadXID(id);
			
			System.out.println("Facultad XID: "+pieza_xid);
			
			return pieza_xid;
		}
		
		@PutMapping("/facultad/{codigo}")
		public Facultad actualizarFacultad(@PathVariable(name="codigo")int id,@RequestBody Facultad facultad) {
			
			Facultad pieza_seleccionado= new Facultad();
			Facultad pieza_actualizado= new Facultad();
			
			pieza_seleccionado= facultadService.facultadXID(id);
			
			pieza_seleccionado.setNombre(facultad.getNombre());
			
			pieza_actualizado = facultadService.actualizarFacultad(facultad);
			
			System.out.println("La facultad actualizada es: "+ pieza_actualizado);
			
			return pieza_actualizado;
		}
		
		@DeleteMapping("/facultad/{codigo}")
		public void eleiminarFacultad(@PathVariable(name="codigo")int id) {
			facultadService.eliminarFacultad(id);
		}
}
