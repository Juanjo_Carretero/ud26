package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.Equipo;
import com.example.demo.service.EquipoServiceImpl;

@RestController
@RequestMapping("/api")
public class EquipoController {
	
	@Autowired
	EquipoServiceImpl equipoService;
	
	@GetMapping("/equipo")
	public List<Equipo> listarEquipos(){
		return equipoService.listarEquipo();
	}
	
	
	@PostMapping("/equipo")
	public Equipo salvarEquipo(@RequestBody Equipo equipo) {
		
		return equipoService.guardarEquipo(equipo);
	}
	
	
	@GetMapping("/equipo/{num_serie}")
	public Equipo equipoXID(@PathVariable(name="num_serie") String id) {
		
		Equipo equipo_xid= new Equipo();
		
		equipo_xid=equipoService.equipoXID(id);
		
		System.out.println("Equipo XID: "+equipo_xid);
		
		return equipo_xid;
	}
	
	@PutMapping("/equipo/{num_serie}")
	public Equipo actualizarEquipo(@PathVariable(name="num_serie")String id,@RequestBody Equipo equipo) {
		
		Equipo equipo_seleccionado= new Equipo();
		Equipo equipo_actualizado= new Equipo();
		
		equipo_seleccionado= equipoService.equipoXID(id);
		
		equipo_seleccionado.setNombre(equipo.getNombre());
		
		equipo_actualizado = equipoService.actualizarEquipo(equipo);
		
		System.out.println("El equipo actualizado es: "+ equipo_actualizado);
		
		return equipo_actualizado;
	}
	
	@DeleteMapping("/equipo/{num_serie}")
	public void eleiminarEquipo(@PathVariable(name="num_serie")String id) {
		equipoService.eliminarEquipo(id);;
	}

}
