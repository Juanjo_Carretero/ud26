package com.example.demo.controller;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder.In;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.Facultad;
import com.example.demo.dto.Investigador;
import com.example.demo.service.InvestigadorServiceImpl;


@RestController
@RequestMapping("/api")
public class InvestigadorController {
	
	@Autowired
	InvestigadorServiceImpl investigadorService;
	
	@GetMapping("/investigadores")
	public List<Investigador> listarInvestigadores(){
		return investigadorService.listarInvestigadores();
	}
	
	@PostMapping("/investigadores")
	public Investigador salvarInvestigadores(@RequestBody Investigador investigadores) {
		return investigadorService.guardarInvestigador(investigadores);
	}
	
	@GetMapping("/investigadores/{dni}")
	public Investigador investigadorXID(@PathVariable(name="dni") String id) {
		
		Investigador investigador_xid= new Investigador();
		
		investigador_xid=investigadorService.investigadorXID(id);
		
		System.out.println("Investigador XID: "+investigador_xid);
		
		return investigador_xid;
	}
	
	@PutMapping("/investigador/{dni}")
	public Investigador actualizarInvestigador(@PathVariable(name="dni")String id,@RequestBody Investigador investigador) {
		
		Investigador investigador_seleccionado= new Investigador();
		Investigador investigador_actualizado= new Investigador();
		
		investigador_seleccionado= investigadorService.investigadorXID(id);
		
		investigador_seleccionado.setNomApels(investigador.getNomApels());
		
		investigador_seleccionado.setFacultad(investigador.getFacultad());
		
		investigador_actualizado = investigadorService.actualizarInvestigador(investigador);
		
		System.out.println("El investigador actualizado es: "+ investigador_actualizado);
		
		return investigador_actualizado;
	}
	
	@DeleteMapping("/investigador/{dni}")
	public void eliminarCurso(@PathVariable(name="dni")String id) {
		investigadorService.eliminarInvestigador(id);
	}
}
