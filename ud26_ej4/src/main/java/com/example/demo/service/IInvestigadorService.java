package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.Investigador;

public interface IInvestigadorService {
	
	public List<Investigador> listarInvestigadores(); //Listar All 
	
	public Investigador guardarInvestigador(Investigador investigador);	//Guarda un Curso CREATE
	
	public Investigador investigadorXID(String id); //Leer datos de un Curso READ
	
	public Investigador actualizarInvestigador(Investigador investigador); //Actualiza datos del Curso UPDATE
	
	public void eliminarInvestigador(String id);// Elimina el Curso DELETE
	
}
